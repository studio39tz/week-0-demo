$(function() {

  console.log('jQuery Loaded...');

  var getUsers = function() {
    $.ajax({
      method: 'get',
      url: '/users'
    }).fail(function(xhr, status) {
      console.log(status);
    }).done(function(result) {
      return result;
    });
  };

  console.log(getUsers());

  var getAllUsers = function() {
    $.get('/users', function(data) {
      console.log('get function');
      console.log(data);
      return data;
    });
  };

  console.log('get all users start');
  console.log(getAllUsers());
  console.log('get all users fin');


  $('#btn').click(function(event) {
    console.log('btn clicked!');
  });

});
