// imports
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    bodyParser = require('body-parser');

// middleware config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

/*
 * Routes
 */
app.get('/', function(req, res) {
  res.sendFile('main.html', {root: './src'});
});

var users = ['Bert', 'Edgar', 'Phillip', 'Wormold', 'Beatrice', 'Gerald', 'Fran', 'Nigel'];

app.get('/users', function(req, res) {
  console.log(req.body);
  res.send(users);
});

app.post('/users', function(req, res) {
  console.log(req.body);
  var name = req.body.name;
  users.push(name);
  res.json(users);
});

// serve static files
app.use(express.static(__dirname + '/public'));

/*
 * Init http Server
 */
var server = http.listen(3000, function () {
    var serverInfo = {
        host: server.address().address,
        port: server.address().port
    };
    console.log(' Server listening on: ' + JSON.stringify(serverInfo));
});
